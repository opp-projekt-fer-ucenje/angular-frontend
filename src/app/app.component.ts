import {Component, OnInit} from '@angular/core';
import {TestService} from "./services/test.service";
import {Person} from "./models/Person";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ucenje';

  people: Person[] = [];

  public form: FormGroup;

  constructor(private test: TestService,
              builder: FormBuilder) {
    this.form = builder.group({
      name: [],
      age: []
    })
  }

  getCounter() {
    return this.test.getCounter();
  }

  ngOnInit() {
    this.loadPeople();
  }

  loadPeople() {
    // šaljemo GET zahtjev na backend servis i onda hendlamo povratne podatke pomoću
    // .then metode.
    // unutar nje definiramo podatke koje primamo kao Person[], tj. array Person modela te nam angular (točnije typescript)
    // sve to točno upakira i spremamo u lokalnu varijablu.
    this.test.getPeople()
      .then((data: Person[]) => {
        this.people = data;
      });
  }

  save() {
    // ako je form validan (niti jedan Validator ne pada na testu)
    if (this.form.valid) {
      // kreiraj novi objekt Person sa vrijednostima iz forma
      const person = new Person(this.form.get('name').value,
        this.form.get('age').value);
      this.test.savePerson(person);
      // reloadaj korisnike
      this.loadPeople();
    }
  }
}
