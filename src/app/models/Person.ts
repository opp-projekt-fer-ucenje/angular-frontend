/**
 * Definiramo model Person koji ima ime i dob.
 * To je ekvivalent PersonDto na backendu.
 */
export class Person {
  constructor(public name: string,
              public age: number) {

  }
}
