import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Person} from "../models/Person";

/**
 * Servis koji u pozadini broji sekunde otkada je aplikacija dignuta i
 * hendla http zahtjeve
 */
@Injectable({
  providedIn: 'root'
})
export class TestService {
  private static readonly URL = "http://localhost:9000/restorani/";

  private counter = 0;

  constructor(private http: HttpClient) {
    // svakih 1000 milisekundi podigni counter za 1
    setInterval(() => {
      this.counter += 1;
    }, 1000);
  }

  public getCounter() {
    return this.counter;
  }

  /**
   * Šaljemo get request na restorani/
   */
  public getPeople(): Promise<any> {
    // .toPromise() je bitna metoda jer bez nje http request nije izveden (osim ako Observable nije subscribean)
    return this.http.get(TestService.URL).toPromise()
  }

  /**
   * Šaljemo post request na restorani/
   * @param person
   */
  public savePerson(person: Person) {
    this.http.post(TestService.URL, person).toPromise();
  }
}
