import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TestService} from "./services/test.service";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  // deklaracije svih komponenata koje smo kreirali
  declarations: [
    AppComponent,
    HomeComponent
  ],
  // importovi eksternih modula koje koristimo
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  // svi servisi koje želimo imati u pozadini
  providers: [TestService],
  // koja je početna komponenta aplikacije
  bootstrap: [AppComponent]
})
export class AppModule { }
